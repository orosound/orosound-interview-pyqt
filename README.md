## Setup

### Create virutal environment

```sh
$ python -m venv venv
```

### Activate virtual environment

```sh
$ source venv/bin/activate
```

### Install dependencies

```sh
$ python -m pip install -Ur src/requirements.txt
```

### Launch app

```sh
$ python src/.../main.py
```